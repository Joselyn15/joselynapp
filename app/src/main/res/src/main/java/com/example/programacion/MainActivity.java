package com.example.programacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button datos;
    Button estudios;
    Button experiencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datos = (Button)findViewById(R.id.idDatos);
        estudios = (Button)findViewById(R.id.idEstudio);
        experiencia = (Button)findViewById(R.id.idExperiencia);

        datos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this, DatosPersonales.class);
                startActivity(ir);
            }
        });

        estudios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this, EstudiosSecundarios.class);
                startActivity(ir);
            }
        });

        experiencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this, ExperienciaLaboral.class);
                startActivity(ir);
            }
        });

    }
}